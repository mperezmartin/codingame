import sys
import math


class Factory:
    def __init__(self, _id):
        self.id = _id
        self.owner = None
        self.cyborgs = None
        self.production = None
        self.neighbors = dict()


class Troop:
    def __init__(self):
        self.owner = None
        self.source = None
        self.dest = None
        self.cyborgs = None
        self.turns = None


class World:
    def __init__(self):
        self.factories = set()
        self.troops = list()


world = World()

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

factory_count = int(raw_input())  # the number of factories
link_count = int(raw_input())  # the number of links between factories
for i in xrange(link_count):
    factory_1, factory_2, distance = [int(j) for j in raw_input().split()]
    f1 = next((f for f in world.factories if f.id == factory_1), Factory(factory_1))
    f1.neighbors[factory_2] = distance
    f2 = next((f for f in world.factories if f.id == factory_2), Factory(factory_2))
    f2.neighbors[factory_1] = distance
    world.factories.add(f1)
    world.factories.add(f2)
# game loop
while True:
    entity_count = int(raw_input())  # the number of entities (e.g. factories and troops)
    for i in xrange(entity_count):
        entity_id, entity_type, arg_1, arg_2, arg_3, arg_4, arg_5 = raw_input().split()
        entity_id = int(entity_id)
        arg_1 = int(arg_1)
        arg_2 = int(arg_2)
        arg_3 = int(arg_3)
        arg_4 = int(arg_4)
        arg_5 = int(arg_5)

    # Write an action using print
    # To debug: print >> sys.stderr, "Debug messages..."


    # Any valid action, such as "WAIT" or "MOVE source destination cyborgs"
    print "WAIT"


